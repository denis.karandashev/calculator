# without imports

# try it over here: https://colab.research.google.com/drive/15QQym1riBqbcTw8dq0ARQyzui0jLK1dn?usp=sharing

flag = True
number_1 = float(input("Введите первое число: "))
arrayOfValues = [number_1]
arrayOfOperators = []
arrayOfResults = [number_1]

while flag == True:
  operator = input("Введите знак арифметического действия: ")

  if operator == "":
    flag = False
    break
  else:
    number = input("Введите следующее число: ")
    arrayOfOperators.append(str(operator))
    arrayOfValues.append(float(number))

  indexOfLastOperator = int(len(arrayOfOperators) - 1)
  indexOfLastValue = int(len(arrayOfValues) - 1)
  indexOfLastResult = int(len(arrayOfResults) - 1)

  if arrayOfOperators[indexOfLastOperator] == "+" and indexOfLastResult >= 0 and number != "":
    temp = arrayOfResults[indexOfLastResult] + float(number)
    arrayOfResults.append(temp) 
    print("Промежуточный результат: ", temp) 
  elif arrayOfOperators[indexOfLastOperator] == "-" and indexOfLastResult >= 0 and number != "":
    temp = arrayOfResults[indexOfLastResult] - float(number)
    arrayOfResults.append(temp) 
    print("Промежуточный результат: ", temp) 
  elif arrayOfOperators[indexOfLastOperator] == "*" and indexOfLastResult >= 0 and number != "":
    temp = arrayOfResults[indexOfLastResult] * float(number)
    arrayOfResults.append(temp) 
    print("Промежуточный результат: ", temp) 
  elif arrayOfOperators[indexOfLastOperator] == "/" and indexOfLastResult >= 0 and number != "":
    if number == "0":
      number = 1
      print("Деление на ноль запрещено!")
    temp = arrayOfResults[indexOfLastResult] / float(number)
    arrayOfResults.append(temp) 
    print("Промежуточный результат: ", temp) 

indexOfLastResult = int(len(arrayOfResults) - 1)
print("Итоговый результат: ", arrayOfResults[indexOfLastResult])